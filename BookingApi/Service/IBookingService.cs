﻿using BookingApi.Model;

namespace BookingApi.Service
{
    public interface IBookingService
    {
        bool AddToCart(Booking booking);
        List<Booking> GetCartByUserId(int id);
        bool Delete(DeleteCart deleteCart);
    }
}
